// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "homeworkGameMode.h"
#include "homeworkHUD.h"
#include "homeworkCharacter.h"
#include "UObject/ConstructorHelpers.h"

AhomeworkGameMode::AhomeworkGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AhomeworkHUD::StaticClass();
}
